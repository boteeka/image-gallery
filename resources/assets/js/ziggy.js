    var Ziggy = {
        namedRoutes: {"image.delete":{"uri":"my-account\/images\/{id}","methods":["DELETE"],"domain":null},"image.search":{"uri":"images\/search\/{term}\/{order?}","methods":["GET","HEAD"],"domain":null},"image.searchown":{"uri":"images\/searchown\/{term}\/{order?}","methods":["GET","HEAD"],"domain":null},"users.list":{"uri":"users\/list","methods":["GET","HEAD"],"domain":null},"users.create":{"uri":"users\/create","methods":["POST"],"domain":null},"users.store":{"uri":"users\/{id}\/store","methods":["POST"],"domain":null},"users.delete":{"uri":"users\/{id}\/delete","methods":["DELETE"],"domain":null}},
        baseUrl: 'http://localhost/',
        baseProtocol: 'http',
        baseDomain: 'localhost',
        basePort: false,
        defaultParameters: []
    };

    if (typeof window.Ziggy !== 'undefined') {
        for (var name in window.Ziggy.namedRoutes) {
            Ziggy.namedRoutes[name] = window.Ziggy.namedRoutes[name];
        }
    }

    export {
        Ziggy
    }
