/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');
//
// /**
//  * The following block of code may be used to automatically register your
//  * Vue components. It will recursively scan this directory for the Vue
//  * components and automatically register them with their "basename".
//  *
//  * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
//  */
//
// // const files = require.context('./', true, /\.vue$/i)
// // files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
//
// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
//
// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */
//
// const app = new Vue({
//     el: '#app'
// });

window.FilePond                           = require('filepond');
window.FilePondPluginImagePreview         = require('filepond-plugin-image-preview');
window.FilePondPluginFileValidateType     = require('filepond-plugin-file-validate-type');
window.FilePondPluginImageExifOrientation = require('filepond-plugin-image-exif-orientation');
require('bootstrap-table');


window.setupSearch = (routename) => {
    const $gridContainer = $('#gridContainer');

    function getSearchResults () {
        const term  = $('#imageSearch').val();
        const order = $('#searchOrder').val();
        $gridContainer.hide();
        $.ajax(
            {
                url: route(routename, {term: term, order: order}),
            }
        ).done((data, textStatus, jqXHR) => {
            $gridContainer.html(data);
            $gridContainer.show();
            const order = $('#searchOrder').val();
            $('.dropdown-item').each((index, element) => {
                if ($(element).data('val') === order) {
                    $('#searchDropdownButton').html($(element).html());
                }
            });
        }).fail(() => {
            $gridContainer.show();
        });
    }

    $gridContainer.on('submit', '#searchForm', (e) => {
        e.stopPropagation();
        e.preventDefault();
        getSearchResults();
    });

    $gridContainer.on('keypress', '#imageSearch', (e) => {
        if (e.which === 13) {
            $('#searchForm').submit();
        }
    });

    $gridContainer.on('click', '.dropdown-item', (e) => {
        $('#searchDropdownButton').html($(e.currentTarget).html());
        $('#searchOrder').val($(e.currentTarget).data('val'));
    })
};
