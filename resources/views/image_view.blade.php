@extends('layouts.app')

@section('content')
    <div class="card">
        <h3 class="card-header">Image</h3>

        <div class="card-body">
            <div class="card shadow-sm">

                @auth
                    @if($image->user->id === auth()->user()->id)
                        <div style="position: absolute; width: 100%;" class="p-4">
                            <div class="float-right">
                                <a href="{{ route('image.load', ['id' => $image->id]) }}" class="btn btn-lg btn-success"
                                   title="Edit"><i class="material-icons">edit </i></a>
                                <a href="#" class="btn btn-lg btn-danger" title="Delete" data-toggle="modal"
                                   data-target="#deleteConfirm"><i class="material-icons">delete </i></a>
                            </div>
                        </div>
                    @endif
                @endauth
                <img class="card-img-top " src="{{ asset('storage/' . $image->path) }}" alt="{{ $image->title  }}">
                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-3">Title</dt>
                        <dd class="col-sm-9">{{ $image->title }}</dd>

                        <dt class="col-sm-3">Uploaded by</dt>
                        <dd class="col-sm-9">{{ $image->getUploaderName() }}</dd>

                        <dt class="col-sm-3">Uploaded on</dt>
                        <dd class="col-sm-9">{{ \Carbon\Carbon::parse($image->uploaded_at)->format('d/m/Y') }}</dd>
                    </dl>
                    <hr>
                    <h3>Comments:</h3>

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if($image->comments)
                        <dl class="row offset-lg-1">
                            <dt class="col-sm-3">{{ auth()->user()->getFullName() }}:</dt>
                            <dd class="col-sm-9">
                                <form method="POST" action="{{ route('comment.store') }}">
                                    @csrf
                                    <input type="hidden" name="image_id" value="{{ $image->id }}">
                                    <textarea name="comment" id="comment" placeholder="Your comment"
                                              class="form-control custom-control {{ $errors->has('comment') ? ' is-invalid' : '' }}"
                                              rows="3"
                                              required
                                              minlength="3"
                                              maxlength="190"

                                    >{{ old('comment') }}</textarea>
                                    @if ($errors->has('comment'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                                    @endif
                                    <div class="mt-3">
                                        <button class="btn btn-primary float-right" id="submit" type="submit">Add
                                            comment
                                        </button>
                                    </div>

                                </form>
                            </dd>
                            @foreach($image->comments as $comment)
                                <dt class="col-sm-3 mt-3 text-muted">
                                    {{ $comment->user->getFullName() }}:
                                    <br>
                                    <small>({{ \Carbon\Carbon::parse($comment->created_at)->format('d/m/Y H:i') }})
                                    </small>
                                </dt>
                                <dd class="col-sm-9 mt-3">{{ $comment->body }}</dd>
                            @endforeach
                        </dl>
                    @endif

                </div>
            </div>

        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="deleteConfirm" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Confirm deletion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Do you want to delete this image?</p>
                    <p>All associated content (comments and ratings) will also be deleted.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="deleteButton">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="deleteError" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title">Error</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>There was an error deleting the image</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        window.addEventListener('load', () => {
            const $comment       = $('#comment');
            const $submit        = $('#submit');
            const $deleteButton  = $('#deleteButton');
            const $deleteConfirm = $('#deleteConfirm');
            const $deleteError   = $('#deleteError');

            $comment.on('keyup', (e) => {
                if (e.currentTarget.value.length >= 3) {
                    $submit.prop('disabled', false);
                } else {
                    $submit.prop('disabled', true);
                }
            });

            $deleteButton.on('click', () => {
                $.ajax(
                    {
                        url:     "{{ route('image.delete', ['id' => $image->id]) }}",
                        type:    'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }
                ).done(function (data, textStatus, jqXHR) {
                    window.history.back();
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    $deleteError.modal('show');
                }).always(function () {
                    $deleteConfirm.modal('hide');
                });
            });
        });
    </script>
@endsection
