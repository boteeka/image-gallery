<div class="d-flex justify-content-end mb-3">
    <div>
        <form id="searchForm">
            <input type="hidden" id="searchOrder" value="{{ isset($order) ? $order : '' }}">
            <div class="form-group ">
                <div class="input-group">
                    <input value="{{ isset($term) ? $term : '' }}" type="text" name="search" id="imageSearch"
                           class="form-control" placeholder="Search"
                           required
                           minlength="3"
                           >
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="submit" id="imageSearchButton">Search</button>
                    </div>
                    <div class="input-group-append">
                        <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"
                                id="searchDropdownButton">Show newest first
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#" data-val="asc">Show oldest first</a>
                            <a class="dropdown-item" href="#" data-val="desc">Show newest first</a>
                        </div>
                    </div>

                </div>
                <div class="form-control-feedback"></div>
            </div>
        </form>
    </div>
</div>
@if(isset($term))
    <div class="col-12 d-flex justify-content-start mb-3">
        <h4>Search results:</h4>
    </div>
@endif
<div class="grid d-flex row flex-wrap justify-content-between align-content-start">
    @forelse($images as $image)
        <div class="col-lg-4 col-sm-12">
            <div class="card mb-2 shadow-sm grid-image">
                @auth
                    @if($image->user->id === auth()->user()->id)
                        <div style="position: absolute; width: 100%;" class="p-1">
                            <div class="float-right">
                                <a href="{{ route('image.load', ['id' => $image->id]) }}" class="btn btn-sm btn-success"
                                   title="Edit"><i class="material-icons">edit </i></a>
                                <a href="#" class="btn btn-sm btn-danger" title="Delete" data-toggle="modal"
                                   data-target="#deleteConfirm" data-id="{{ $image->id }}"><i class="material-icons">delete </i></a>
                            </div>
                        </div>
                    @endif
                @endauth
                <a href="{{ asset('storage/' . $image->path) }}" target="_blank" class="rounded-top">
                    <img class="img-fluid" src="{{ asset('storage/' . $image->path) }}" alt="{{ $image->title  }}">
                </a>
                <div class="card-body d-flex align-items-end">
                    <div class="flex-fill">
                        <p class="card-text">
                            {{ $image->title  }}
                            <br>
                            <small class="text-muted">{{ count($image->comments) }} comment(s) /
                                rating {{ $image->getAverageRating() }}</small>
                        </p>
                        <a href="{{ route('image.view', ['id' => $image->id]) }}"
                           class="btn btn-sm btn-outline-dark float-right">View image</a>
                    </div>
                </div>
            </div>
        </div>
    @empty
        <div class="col-12 d-flex justify-content-center">
            <p class="text-center">No images were found</p>
        </div>

    @endforelse
</div>

@unless(isset($nolinks) && $nolinks)
    <div class="pt-5 d-flex justify-content-center">
        {{ $images->links() }}
    </div>
@endunless

<div class="modal" tabindex="-1" role="dialog" id="deleteConfirm" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirm deletion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you want to delete this image?</p>
                <p>All associated content (comments and ratings) will also be deleted.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="deleteButton">Delete</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="deleteError" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title">Error</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>There was an error deleting the image</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    @parent
    <script>
        window.onload = () => {
            const $deleteButton  = $('#deleteButton');
            const $deleteConfirm = $('#deleteConfirm');
            const $deleteError   = $('#deleteError');

            $deleteButton.on('click', () => {
                const id = $deleteButton.data('id');
                $.ajax(
                    {
                        url:     route('image.delete', { id: id }),
                        type:    'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }
                ).done(function (data, textStatus, jqXHR) {
                    window.location.reload();
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    $deleteError.modal('show');
                }).always(function () {
                    $deleteConfirm.modal('hide');
                });
            });
        };
    </script>
@endsection
