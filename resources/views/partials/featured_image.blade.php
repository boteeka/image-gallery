<div class="card">
    <h3 class="card-header">Featured Image</h3>

    <div class="card-body">
        @if($featuredImage)
            <div class="card mb-2 shadow-sm">
                <a href="{{ asset('storage/' . $featuredImage->path) }}" target="_blank">
                    <img class="card-img-top" src="{{ asset('storage/' . $featuredImage->path) }}" alt="{{ $featuredImage->title  }}">
                </a>
                <div class="card-body">
                    <p class="card-text">
                        {{ $featuredImage->title  }}
                        <br>
                        <small class="text-muted">{{ count($featuredImage->comments) }} comment(s) /
                            rating {{ $featuredImage->getAverageRating() }}</small>
                    </p>
                    <a href="{{ route('image.view', ['id' => $featuredImage->id]) }}"
                       class="btn btn-sm btn-outline-dark float-right">View image</a>
                </div>
            </div>
        @endif
    </div>
</div>
