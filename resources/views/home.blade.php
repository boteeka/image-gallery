@extends('layouts.app')

@section('content')
    <div class="card">
        <h3 class="card-header">Images</h3>

        <div class="card-body">

            <div id="gridContainer">
                @include('partials.paginated_image_grid', ['images' => $images])
            </div>


        </div>
    </div>
@endsection

@section('scripts')
    @routes
    <script>
        window.addEventListener('load',() => {
            setupSearch('image.search');
        });
    </script>
@endsection
