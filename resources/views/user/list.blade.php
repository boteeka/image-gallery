@extends('layouts.app')

@section('content')
    <div class="card">
        <h3 class="card-header">Users</h3>

        <div class="card-body">
            <div class="alert alert-success d-none" role="alert" id="alertSuccess">

            </div>

            <div class="alert alert-danger d-none" role="alert" id="alertError">

            </div>

            <form id="editForm" class="d-none">
                <input type="hidden" name="id">
                <div class="form-group row">
                    <label for="title" class="col-sm-4 col-form-label text-md-right">Firstname</label>
                    <div class="col-md-6">
                        <input type="text"
                               class="form-control" name="first_name"
                               required autofocus maxlength="255" minlength="2"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="title" class="col-sm-4 col-form-label text-md-right">Lastname</label>
                    <div class="col-md-6">
                        <input type="text"
                               class="form-control" name="last_name"
                               required maxlength="255" minlength="2"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="title" class="col-sm-4 col-form-label text-md-right">Email</label>
                    <div class="col-md-6">
                        <input type="email"
                               class="form-control" name="email"
                               required/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="title" class="col-sm-4 col-form-label text-md-right">Phone</label>
                    <div class="col-md-6">
                        <input type="text"
                               class="form-control" name="phone"
                               required minlength="6"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-4">
                        <button type="submit" id="saveButton" class="btn btn-primary">Save</button>
                        <button type="button" id="cancelButton" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </form>

            <table class="table table-responsive-sm table-striped table-hover" id="userTable">
                <thead>
                <tr>
                    <th scope="col">Firstname</th>
                    <th scope="col">Lastname</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

            <div class="mt-3">
                <button class="btn btn-primary float-right" id="addUserButton">Add new user</button>
            </div>


        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="deleteConfirm" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Confirm deletion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Do you want to delete this user?</p>
                    <p>All associated content (images, comments and ratings) will also be deleted.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="deleteButton">Delete</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @routes
    <script>
        window.onload = () => {
            const $userTable     = $('#userTable');
            const $deleteButton  = $('#deleteButton');
            const $deleteConfirm = $('#deleteConfirm');
            const $alertSuccess  = $('#alertSuccess');
            const $alertError    = $('#alertError');
            const $editForm      = $('#editForm');
            const $addUserButton = $('#addUserButton');

            $alertError.hide().removeClass('d-none');
            $alertSuccess.hide().removeClass('d-none');
            $editForm.hide().removeClass('d-none');

            function showTableOnly () {
                $alertError.hide();
                $alertSuccess.hide();
                $editForm.hide();
                $userTable.parents('.bootstrap-table').show();
                $addUserButton.show();
            }

            function showFormOnly () {
                $userTable.parents('.bootstrap-table').hide();
                $addUserButton.hide();
                $alertError.hide();
                $alertSuccess.hide();
                $editForm.show();
            }

            function showSuccess (message) {
                $alertError.hide();
                $alertSuccess.html(message).show();
            }

            function showError (message) {
                $alertError.html(message).show();
                $alertSuccess.hide();
            }

            function clearForm () {
                $editForm.find('input[name]').each((index, element) => {
                    $(element).val(null);
                });
            }

            function populateForm (data) {
                clearForm();
                $editForm.find('input[name]').each((index, element) => {
                    if (data.hasOwnProperty($(element).attr('name'))) {
                        $(element).val(data[$(element).attr('name')]);
                    }
                });
            }

            function getFormData () {
                const data = {};
                $editForm.find('input[name]').each((index, element) => {
                    if ($(element).val()) {
                        data[$(element).attr('name')] = $(element).val();
                    }
                });

                return data;
            }

            $deleteButton.on('click', () => {
                showTableOnly();
                const id = $deleteConfirm.data('id');
                $.ajax(
                    {
                        url:     route('users.delete', {id: id}),
                        method:  'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }
                ).done((data, textStatus, jqXHR) => {
                    showSuccess('User has been deleted.');
                    $deleteConfirm.modal('hide');
                    $userTable.bootstrapTable('refresh');
                }).fail((jqXHR, textStatus, errorThrown) => {
                    showError('User deletion failed.');
                    $deleteConfirm.modal('hide');
                });
            });

            $userTable.on('click', '.action-delete', (e) => {
                const id = $(e.currentTarget).attr('data-id');
                $deleteConfirm.data('id', id);
                $deleteConfirm.modal('show');
            });

            $userTable.on('click', '.action-edit', (e) => {
                const id   = $(e.currentTarget).attr('data-id');
                const data = $userTable.bootstrapTable('getRowByUniqueId', id);
                populateForm(data);
                showFormOnly();
            });

            $editForm.on('click', '#cancelButton', () => {
                clearForm();
                showTableOnly();
            });

            $editForm.on('submit', (e) => {
                e.preventDefault();
                e.stopPropagation();
                const formData = getFormData();
                $.ajax(
                    {
                        url:     formData.id ? route('users.store', formData.id) : route('users.create'),
                        data:    formData,
                        method:  'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }
                ).done((data, textStatus, jqXHR) => {
                    showTableOnly();
                    showSuccess('User has been saved.');
                    $userTable.bootstrapTable('refresh');
                }).fail((jqXHR, textStatus, errorThrown) => {
                    showError('User could not be saved.');
                });
            });

            $addUserButton.on('click', () => {
                populateForm(
                    {
                        last_name:  '',
                        first_name: '',
                        email:      '',
                        phone:      ''
                    }
                );
                showFormOnly();
            });


            function actionFormatter (value, row, index) {
                return [
                    `<button class="btn btn-sm btn-outline-success action-edit" title="Edit" data-id="${row.id}"><i class="material-icons">edit </i></button>`,
                    `<button class="btn btn-sm btn-outline-danger action-delete" title="Delete" data-id="${row.id}"><i class="material-icons">delete </i></button>`
                ].join('')
            }

            $userTable.bootstrapTable('destroy').bootstrapTable(
                {
                    url:      "{{ route('users.list') }}",
                    search:   true,
                    uniqueId: 'id',
                    columns:  [
                        {
                            field:    'first_name',
                            title:    'Firstname',
                            sortable: true,
                        },
                        {
                            field:    'last_name',
                            title:    'Lastname',
                            sortable: true,
                        },
                        {
                            field:    'email',
                            title:    'Email',
                            sortable: true,
                        },
                        {
                            field:    'phone',
                            title:    'Phone',
                            sortable: true,
                        },
                        {
                            field:     'action',
                            title:     '',
                            sortable:  false,
                            align:     'center',
                            formatter: actionFormatter
                        }
                    ]
                }
            );
        };
    </script>
@endsection
