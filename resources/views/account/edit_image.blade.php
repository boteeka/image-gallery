@extends('layouts.app')

@section('content')
    <div class="card">
        <h3 class="card-header">Edit image</h3>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" action="{{ route('image.store') }}">
                @csrf

                <input type="hidden" name="id" id="imageId" value="{{ $image->id }}">

                <div class="form-group row ">
                    <label for="title" class="col-sm-4 col-form-label text-md-right">{{ __('File Upload') }}</label>
                    <div class="col-md-6">
                        <div class="">
                            <div id="file"></div>
                        </div>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="title" class="col-sm-4 col-form-label text-md-right">Title</label>
                    <div class="col-md-6">
                        <input id="title" type="text"
                               class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title"
                               value="{{ old('title') ?? $image->title }}" required autofocus minlength="3"/>
                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-4">
                        <button type="submit" id="saveButton" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // This event listener is needed, because scripts are loaded with defer
        document.addEventListener('FilePond:loaded', e => {
            FilePond.registerPlugin(
                FilePondPluginImagePreview,
                FilePondPluginFileValidateType,
                FilePondPluginImageExifOrientation
            );

            FilePond.setOptions({
                                    multiple:      false,
                                    instantUpload: true,
                                    allowRevert:   true,
                                    required:      true,
                                    server:        {
                                        url:     "{{ route('image.upload', ['load' => $image->id]) }}",
                                        process: {
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            }
                                        },
                                        revert:  {
                                            url:     "{{ route('image.revert') }}",
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            }
                                        }
                                    }
                                });
            const inputElement = document.querySelector('#file');
            const pond         = FilePond.create(inputElement, {
                files: [
                    {
                        // the server file reference
                        source: '{{ $image->id }}',
                        load: false,
                        options: {
                            type: 'local',
                        }
                    }
                ]
            });
            pond.on('processfile', (e, f) => {
                console.log(f.serverId);
                $('#imageId').val(f.serverId);
                $('#saveButton').prop('disabled', false);
            });
            pond.on('removefile', e => {
                $('#saveButton').prop('disabled', true);
            });
        });
    </script>
@endsection
