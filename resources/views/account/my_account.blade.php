@extends('layouts.app')

@section('content')
    <div class="card">
        <h3 class="card-header">My Images</h3>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="d-flex justify-content-end mb-2">
                <a href="{{ route('image') }}" class="btn-primary btn" role="button">Upload Image</a>
            </div>

            <div id="gridContainer">
                @include('partials.paginated_image_grid', ['images' => $images])
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    @routes
    <script>
        window.addEventListener('load', () => {
            setupSearch('image.searchown');
        });
    </script>
@endsection
