<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name'       => 'Thomas A. Anderson',
                    'first_name' => 'Thomas',
                    'last_name'  => 'Anderson',
                    'phone'      => '+1 925-228-8807',
                    'email'      => 'admin@example.com',
                    'password'   => bcrypt('adminsecret'),
                    'is_admin'   => true,
                ],
                [
                    'name'       => 'Emma C. Armstrong',
                    'first_name' => 'Emma',
                    'last_name'  => 'Armstrong',
                    'phone'      => '+1 305-574-1795',
                    'email'      => 'EmmaCArmstrong@jourrapide.com',
                    'password'   => bcrypt('secret'),
                    'is_admin'   => false,
                ],
                [
                    'name'       => 'Glenn H. Babin',
                    'first_name' => 'Glenn',
                    'last_name'  => 'Babin',
                    'phone'      => '+1 216-970-0721',
                    'email'      => 'GlennHBabin@dayrep.com',
                    'password'   => bcrypt('secret'),
                    'is_admin'   => false,
                ],
                [
                    'name'       => 'Lucy Gardiner',
                    'first_name' => 'Lucy',
                    'last_name'  => 'Gardiner',
                    'phone'      => '+1 602-414-8004',
                    'email'      => 'LucyGardiner@armyspy.com',
                    'password'   => bcrypt('secret'),
                    'is_admin'   => false,
                ],

            ]
        );
    }
}
