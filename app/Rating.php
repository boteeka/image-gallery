<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Rating
 *
 */
class Rating extends Model
{
    /**
     * Get the image that owns the rating.
     */
    public function image()
    {
        return $this->belongsTo(Image::class);
    }
}
