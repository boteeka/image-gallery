<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Image
 *
 */
class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'path',
    ];

    /**
     * Get the user that owns the image.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the comments for the image.
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Get the ratings for the image.
     */
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    /**
     * Get the average rating of the image.
     */
    public function getAverageRating(): float
    {
        return $this->ratings->count() ? $this->ratings->avg('value') : 0;
    }

    public function getUploaderName(): string
    {
        return $this->user->getFullName();
    }

}
