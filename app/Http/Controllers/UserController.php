<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        return view('user.list');
    }

    public function list(Request $request)
    {
        $users = User::all();

        return response()->json($users);
    }

    public function delete(Request $request)
    {
        $id = $request->route('id');
        User::findOrFail($id)->delete();
    }

    public function store(Request $request)
    {
        $id        = $request->route('id');
        $validator = Validator::make(
            $request->all(),
            [
                'first_name' => 'required:max:255|min:3',
                'last_name'  => 'required:max:255|min:3',
                'email'      => 'required:email',
                'phone'      => 'required:max:255|min:3',
            ]
        );
        $validator->validate();

        if ($validator->fails()) {
            return response()->json($validator, 400);
        }

        $user             = User::findOrFail($id);
        $user->first_name = $request->get('first_name');
        $user->last_name  = $request->get('last_name');
        $user->email      = $request->get('email');
        $user->phone      = $request->get('phone');
        $user->saveOrFail();
    }

    public function create(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'first_name' => 'required:max:255|min:3',
                'last_name'  => 'required:max:255|min:3',
                'email'      => 'required|email',
                'phone'      => 'required:max:255|min:3',
            ]
        );
        $validator->validate();

        if ($validator->fails()) {
            return response()->json($validator, 400);
        }

        $user           = new User($request->all());
        $user->name     = $user->getFullName();
        $user->password = bcrypt('secret');
        $user->is_admin = false;
        $user->saveOrFail();
    }
}
