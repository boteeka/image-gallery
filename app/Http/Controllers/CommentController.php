<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $image = Image::findOrFail($request->get('image_id'));

        $validator = Validator::make(
            $request->all(),
            [
                'comment' => 'required:max:190|min:3',
            ]
        );
        $validator->validate();

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $comment = new Comment([
            'body' => $request->get('comment')
        ]);

        $comment->image()->associate($image);
        $comment->user()->associate(auth()->user());
        $comment->saveOrFail();

        return back()->with('status', 'Your comment has been saved.');
    }
}
