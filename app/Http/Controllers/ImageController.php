<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ImageController extends Controller
{

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $images = auth()->user()->images()->with(['ratings'])->paginate(9);

        return view('account/my_account', ['images' => $images]);
    }

    public function view(Request $request)
    {
        $id    = $request->route('id');
        $image = Image::findOrFail($id);

        return view('image_view', ['image' => $image]);
    }

    public function add()
    {
        return view('account/new_image');
    }

    public function store(Request $request)
    {
        $id = $request->get('id');

        $validator = Validator::make(
            $request->all(),
            [
                'title' => 'required:max:255|min:5',
            ]
        );
        $validator->validate();

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $image        = Image::findOrFail($id);
        $image->title = $request->get('title');
        $image->save();

        return redirect()->route('my_images')->with('status', 'Your image has been saved.');
    }

    public function upload(Request $request)
    {
        $id = $request->get('load');

        if ($request->method() === 'GET') {
            $id = (int) $id;

            $image  = Image::findOrFail($id);
            $finfo  = new \finfo(FILEINFO_MIME_TYPE);
            $buffer = Storage::disk('public')->get($image->path);

            return response(
                $buffer,
                200,
                [
                    'Content-Disposition' => sprintf('inline; filename="%s"', basename($image->path)),
                    'Content-Type'        => $finfo->buffer($buffer),
                ]
            );

        }

        $path = $request->file('filepond')->store('images', 'public');

        if (null !== $id) {
            // Image exists, we're just updating the path
            $id          = (int) $id;
            $image       = Image::findOrFail($id);
            $image->path = $path;
            $image->saveOrFail();
        } else {
            // New image
            $image = auth()->user()->images()->create(
                [
                    'path'  => $path,
                    'title' => 'temp',
                ]
            );
        }

        return response($image->id);
    }

    public function revert(Request $request)
    {
        $id = (int) $request->getContent();
        Image::findOrFail($id)->delete();
    }

    public function delete(Request $request)
    {
        $id    = $request->route('id');
        $image = Image::find($id);

        if (null === $image) {
            return response()->json(
                [
                    'success' => false,
                    'error'   => 'Image doesn\'t exist',
                ],
                404
            );
        }

        if (true !== $image->delete()) {
            return response()->json(
                [
                    'success' => false,
                    'error'   => 'Error deleting image',
                ],
                500
            );
        }

        return response()->json(
            [
                'success' => true,
                'error'   => null,
            ]
        );
    }

    public function load(Request $request)
    {
        $id    = $request->route('id');
        $image = Image::findOrFail($id);

        return view('account/edit_image', ['image' => $image]);
    }

    public function search(Request $request)
    {
        $term  = $request->route('term');
        $order = $request->route('order');

        $query1 = Image::where('title', 'LIKE', sprintf('%%%s%%', $term))
            ->with(['ratings'])
            ->orderBy('uploaded_at', $order)
            ->get();

        $query2 = Image::orderBy('uploaded_at', $order)
            ->leftJoin('users', 'users.id', '=', 'images.user_id')
            ->where('users.first_name', 'LIKE', sprintf('%%%s%%', $term))
            ->orWhere('users.last_name', 'LIKE', sprintf('%%%s%%', $term))
            ->get();

        $images = new Collection($query1->all());
        $images = $images->merge($query2->all());

        return view(
            'partials.paginated_image_grid',
            [
                'images'  => $images,
                'nolinks' => true,
                'term'    => $term,
                'order'   => $order,
            ]
        );
    }

    public function searchown(Request $request)
    {
        $term  = $request->route('term');
        $order = $request->route('order');

        // When searching in own images, matching the uploader's name doesn't make much sense, so it's omitted

        $images = auth()->user()->images()->where('title', 'LIKE', sprintf('%%%s%%', $term))
            ->orderBy('uploaded_at', $order)
            ->with(['ratings'])
            ->get();

        return view(
            'partials.paginated_image_grid',
            [
                'images'  => $images,
                'nolinks' => true,
                'term'    => $term,
                'order'   => $order,
            ]
        );
    }
}
