<?php

namespace App\Observers;

use App\Image;
use Illuminate\Support\Facades\Storage;

class ImageObserver
{
    /**
     * Handle the Image "created" event.
     *
     * @param  \App\Image $image
     *
     * @return void
     */
    public function created(Image $image)
    {
        //
    }

    /**
     * Handle the Image "updated" event.
     *
     * @param  \App\Image $image
     *
     * @return void
     */
    public function updated(Image $image)
    {
        //
    }

    /**
     * Handle the Image "deleted" event.
     *
     * @param  \App\Image $image
     *
     * @return void
     */
    public function deleted(Image $image)
    {
        Storage::disk('public')->delete($image->path);
    }
}
