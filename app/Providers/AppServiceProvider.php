<?php

namespace App\Providers;

use App\Image;
use Illuminate\Support\ServiceProvider;
use App\Observers\ImageObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Image::observe(ImageObserver::class);
        view()->composer(
            'partials.featured_image',
            function ($view) {
                view()->share(
                    'featuredImage',
                    Image::with('comments')->inRandomOrder()->first()

                );
            }
        );
    }
}
