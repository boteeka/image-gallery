<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Homepage, paginated list of all images
Route::get('/', 'HomeController@index')
    ->name('homepage');

// New image form
Route::get('my-account/images/new', 'ImageController@add')
    ->name('image')
    ->middleware('auth');

// Store new image data
Route::post('my-account/images/store', 'ImageController@store')
    ->name('image.store')
    ->middleware('auth');

// New image upload handler
Route::post('my-account/images/upload', 'ImageController@upload')
    ->name('image.upload')
    ->middleware('auth');

// Load uploaded file
Route::get('my-account/images/upload', 'ImageController@upload')
    ->name('image.upload-load')
    ->middleware('auth');

// New image upload revert
Route::delete('my-account/images/revert', 'ImageController@revert')
    ->name('image.revert')
    ->middleware('auth');

// Load existing image
Route::get('my-account/images/{id}', 'ImageController@load')
    ->name('image.load')
    ->middleware('auth');

// Delete image
Route::delete('my-account/images/{id}', 'ImageController@delete')
    ->name('image.delete')
    ->middleware('auth');

// Account image list
Route::get('my-account/images', 'ImageController@index')
    ->name('my_images')
    ->middleware('auth');

// Store new comment
Route::post('/images/comments/new', 'CommentController@store')
    ->name('comment.store')
    ->middleware('auth');

// Search images
Route::get('/images/search/{term}/{order?}', 'ImageController@search')
    ->name('image.search')
    ->defaults('order', 'desc');

// Search user's own images
Route::get('/images/searchown/{term}/{order?}', 'ImageController@searchown')
    ->name('image.searchown')
    ->defaults('order', 'desc')
    ->middleware('auth');

// View details of a single image
Route::get('/images/{id}', 'ImageController@view')
    ->name('image.view');

// Users page
Route::get('/users', 'UserController@index')
    ->name('users')
    ->middleware('auth')
    ->middleware('can:access-users');

// List users
Route::get('/users/list', 'UserController@list')
    ->name('users.list')
    ->middleware('auth')
    ->middleware('can:access-users');

// Store user
Route::post('/users/create', 'UserController@create')
    ->name('users.create')
    ->middleware('auth')
    ->middleware('can:access-users');

// Store user
Route::post('/users/{id}/store', 'UserController@store')
    ->name('users.store')
    ->middleware('auth')
    ->middleware('can:access-users');

// Delete user
Route::delete('/users/{id}/delete', 'UserController@delete')
    ->name('users.delete')
    ->middleware('auth')
    ->middleware('can:access-users');

// Disable auth registration functionality
Auth::routes(['register' => false]);

